
Informacion: 

Consumir data de  https://football-data.org.


Problemas:

Ya que las peticiones son 10 cada minuto, no he podido realizar una union de todos los jugadores,
por ahora solo estara listando a los jugadores de un equipo definido,

Este problema tambien se encuentra en teams, ya que son varias competiciones, hacer union de los teams, 
me daria un error de api por las limitacion que tiene una cuenta free y no podria pobrar.



Solucion: 

He utilizado la libreria guzzlehttp/guzzle para realiza las peticiones http

Creacion de la clase Football,
esta clase actua como servicio, para poder hacer las peticiones a la url antes mencionada,
la cual facilitara cualquier peticion que se haga a la url, ya que contiene un prefijo , id y sufijo, 
ya que todos sus end points tiene en el mismo patron.

Uso de la clase Football,
he creado un constructor en el Controller base que nos da ya definido laravel, el cual hace que cualquier
defiicion dentro del esta , pueda ser usado en cualquier controlador extiendo de Controller


Como se puede apreciar, se han separado cada peticion por controladores, para poder tener un patron MVC
cada controllador es llamado por el nombre de las paticiones que se usaran,
 - Competition
 - Player
 - Team
 
 Asi si un proyecto crece, se pueda cambiar o modificar la funcion con mayor facilidad,
 
 Tambien se han validado todos los posibles casos, con un try catch a las peticiones que se hagan a la api
 
 Para las rutas del api
 las he agrupado con el prefijo que manda la peticion
 ya que estas podrian aumentar y sin agruparlas seria un poco tedioso hacer un mantenimiento.
 recordemos que las peticiones pueden ser GET, POST, PUT, DELETE
 
 






