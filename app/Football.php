<?php

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;

class Football
{
    //
    function requestData($prefix, $id = "", $sufix ="", $options= ""){
        $path =  "http://api.football-data.org/v2";
        $url = "{$path}/{$prefix}/{$id}/{$sufix}?$options";
        $header =  ['headers' => ['X-Auth-Token' => '11b7815de27140df9afaed3273525c27']];
        $client = new Client();
        try {
            $promise = $client->request('GET', $url, $header);
            $res = $promise->getBody()->getContents();
        } catch (GuzzleException $e) {
            $res= $e->getMessage();
        }
        return $res;
    }
}
