<?php

namespace App\Http\Controllers;

use App\Football;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

class CompetitionController extends Controller
{

    public function showAll () {
       return $this->football->requestData('competitions');
    }

    public function show ($id) {
        return $this->football->requestData('competitions', $id);
    }


}
