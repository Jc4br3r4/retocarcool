<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class PlayerController extends Controller
{
    //
    public function showAll() {
        $team = $this->football->requestData('teams', 1107);
        $data  = json_decode($team);
        return $data->squad;
    }
}
