<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Psr\Http\Message\ResponseInterface;

class TeamController extends Controller
{

    public function showAll () {
        $date = new Carbon();
        $teams = $this->football->requestData('competitions', $date->year , 'teams');
        $data  = json_decode($teams);
        return $data->teams;
    }

    public function show ($id) {
        $team = $this->football->requestData('teams', $id);
        return $team;
    }


}
