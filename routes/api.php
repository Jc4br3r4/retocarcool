<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix' => 'competitions'], function() {
    Route::get('/', 'CompetitionController@showAll')->name('compentitions');
    Route::get('/{id}', 'CompetitionController@show')->name('compentition');
});

Route::group(['prefix' => 'team'], function() {
    Route::get('/', 'TeamController@showAll')->name('teams');
    Route::get('/{id}', 'TeamController@show')->name('team');
});

Route::group(['prefix' => 'players'], function() {
    Route::get('/', 'PlayerController@showAll')->name('players');
});
